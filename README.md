# Installation 
1. run `make install`
2. run `make all`

# Example JS/TS gRPC-Web Implementation
The source code is available under the `ts` directory. Inside the `ts/src` directory are `index.ts` and `Services.ts` files. `Services.ts` wraps all of the grpc service endpoints into a single class to be used for simplying the interface. `index.ts` shows an example of using the `Services` class to make a grpc call to the PatientService. 

## Running the Example
1. Install the node dependencies
```
$ npm install
```
2. Run webpack to generate the browser friendly js code
```
$ npm run webpack:example
```
3. Make sure to run the `pts` in the

4. Now, open a browser and load the `index.html` page. For example, using firefox:
```
cd ts
firefox index.html
```
5. Open the console output and see the result of the request.