#############################################################
#
# EKLINIK Message/Service Protos generator
#	Author: Joao Baiense
#  Created: 2018-09-16
#
# Makefile for generating proto code in JS/TS and Go 
#
# NOTE:
# 	* Make sure that ${GOBIN} is defined and that 
#		`protoc-gen-go` is installed
#
##############################################################

##############################################################
# Define Make System Variables 
##############################################################

ENVIRONMENT := $(shell basename $(dir $(abspath $(dir $$PWD))))
PWD := $(shell pwd)
OUT_DIR := dist

JS_OUT_DIR := $(OUT_DIR)/ts
TS_OUT_DIR := $(JS_OUT_DIR)
GO_OUT_DIR := ../
DART_OUT_DIR := $(OUT_DIR)/dart
RELEASE_DIR := $(OUT_DIR)/releases

PROTO_DIR := proto

COMMON := $(PROTO_DIR)/common
PTS := $(PROTO_DIR)/pts
RCS := $(PROTO_DIR)/rcs
RTS := $(PROTO_DIR)/rts
PHS := $(PROTO_DIR)/phs
LAS := $(PROTO_DIR)/las
TSS := $(PROTO_DIR)/tss
LGS := $(PROTO_DIR)/lgs
CSS := $(PROTO_DIR)/css

ifeq ($(ENVIRONMENT),node_modules)
	PROTOC_GEN_TS_PATH := ../.bin/protoc-gen-ts
else
	PROTOC_GEN_TS_PATH := node_modules/.bin/protoc-gen-ts
endif

# DART 
DART_PLUGIN := dart/protoc-plugin/bin/protoc-gen-dart

# PROTOC Build options
PROTOC_OPTIONS := --plugin="protoc-gen-ts=$(PROTOC_GEN_TS_PATH)" \
		--plugin="protoc-gen-go=$(GOBIN)/protoc-gen-go" \
		--plugin="protoc-gen-dart=$(DART_PLUGIN)" \
		-I $(PROTO_DIR) \
		--go_out="plugins=grpc:$(GO_OUT_DIR)" \
		--dart_out="grpc:$(DART_OUT_DIR)"


#		--js_out="import_style=commonjs,binary:$(JS_OUT_DIR)" \
#		--ts_out="service=true:$(TS_OUT_DIR)" \

##############################################################
# all:
# 	Runs the default generation 
##############################################################
.PHONY: all
all: dir proto


##############################################################
# install:
# 	pulls in all the npm project dependencies
##############################################################
.PHONY: install 
install: go-install
	cd dart/protoc-plugin/ && pub install
	#npm install # commented out for now

##############################################################
# go-install:
# 	pulls in all the go project dependencies
##############################################################
.PHONY: go-install
go-install:
	go get github.com/grpc-ecosystem/go-grpc-middleware
	go get github.com/grpc-ecosystem/go-grpc-middleware/logging/logrus
	go get github.com/grpc-ecosystem/go-grpc-prometheus
	go get github.com/improbable-eng/grpc-web/go/grpcweb
	go get github.com/mwitkow/go-conntrack
	go get github.com/mwitkow/grpc-proxy/proxy
	go get github.com/spf13/pflag
	go get github.com/golang/protobuf/protoc-gen-go

##############################################################
# dir:
# 	creates the output directories if they don't already exist
##############################################################
.PHONY: dir
dir :
	mkdir -p $(JS_OUT_DIR) $(TS_OUT_DIR) $(GO_OUT_DIR) $(DART_OUT_DIR) $(RELEASE_DIR)

##############################################################
# proto:
# 	runs proto generation using plugins
##############################################################
.PHONY: proto
proto: common pts rcs rts phs las tss lgs css

.PHONY: common
common: 
	protoc $(PROTOC_OPTIONS) $(COMMON)/common.proto


.PHONY: pts
pts: 
	protoc  $(PROTOC_OPTIONS) $(PTS)/pts.proto


.PHONY: rcs
rcs: 
	protoc $(PROTOC_OPTIONS) $(RCS)/rcs.proto


.PHONY: rts
rts:
	protoc $(PROTOC_OPTIONS) $(RTS)/rts.proto

.PHONY: phs
phs:
	protoc $(PROTOC_OPTIONS) $(PHS)/phs.proto

.PHONY: las
las:
	protoc $(PROTOC_OPTIONS) $(LAS)/las.proto

.PHONY: tss
tss:
	protoc $(PROTOC_OPTIONS) $(TSS)/tss.proto

.PHONY: lgs
lgs:
	protoc $(PROTOC_OPTIONS) $(LGS)/lgs.proto

.PHONY: css
css:
	protoc $(PROTOC_OPTIONS) $(CSS)/css.proto

.PHONY: zip 
zip:
	rm -f $(RELEASE_DIR)/release.tar.gz
	tar --exclude='$(RELEASE_DIR)' -czvf $(RELEASE_DIR)/release.tar.gz ./dist 
##############################################################
# clean:
# 	Removes the generated directories
##############################################################
.PHONY: clean
clean:
	rm -r $(OUT_DIR)

##############################################################
# help:
# 	Shows available make commands 
##############################################################

.PHONY: help
help:
	@echo " "
	@echo "Available make commands:"
	@echo "\t" 'all'
	@echo "\t" 'install'
	@echo "\t" 'go-install'
	@echo "\t" 'dir'
	@echo "\t" 'proto - Generates typescript and go proto helpers'
	@echo "\t" 'clean'
	@echo " "