
import {Services, patient_pb} from "./Services";
// in the eklinik-web project, would be importing as
// import {Services, patient_pb, ***} from "./node_modules/eklinik_proto/ts/src/Services";


// Initialize Services
let services = new Services();


// Let's test the register request in patient service
//  Register takes in `patientShort`
//
// message PatientShort {
//     int32 id = 1;
//     Name name = 2;
//     Birthdate birthdate = 3;
//     repeated Relative relatives = 4;
// }
//

let name = new patient_pb.Name();
name.setFirstname('Mr.');
name.setLastname('Potato');

let birthdate = new patient_pb.Birthdate();
birthdate.setDay(1);
birthdate.setMonth(2);
birthdate.setYear(1992);

let patientShort = new patient_pb.PatientShort();
patientShort.setName(name);
patientShort.setBirthdate(birthdate);
// Fields are optional, meaning you don't have to fill everything out
//  before sending the request. But, for the most part, we should be filling
//  out every field

// - not sending relatives in this example... assuming patient came alone

services.patient.register(patientShort, (error, message)=>{
  
  console.log(error); // need to do better error checking
  if (message != null) {

    console.log(message.toObject());
    let id: number =  message.getId();
    // Convert to HEX before showing to user 
    //// but when sending to server, type must be int
    let idHex: string = id.toString(16).toUpperCase();
    console.log('Registered patient with id: ' + idHex)
  
  }
  
});