
//import {grpc} from "grpc-web-client";

// Import code-generated data structures.
import * as service from "../../dist/ts/pts/pts_pb_service";
import * as pts_pb from "../../dist/ts/pts/pts_pb";

// export this so everyone can have access to the proto getters/setters
//  without needing to import the _pb files directly
export let patient_pb = pts_pb;

export class Services {

    // need to load IP:PORT from a config file
    // -> because protocol is using grpc-web, won't be able to do proxying 
    //      with nginx or with gowebproxy. Might be able to if we write
    //      a custom parser... maybe in the future 
    patient = new service.PatientClient('http://localhost:60051');

    constructor() {
        // nothing to do .. yet 
    }
}

/*
import {grpc} from "grpc-web-client";
import {BookService} from "../_proto/examplecom/library/book_service_pb_service";
import {QueryBooksRequest, Book, GetBookRequest} from "../_proto/examplecom/library/book_service_pb";

declare const USE_TLS: boolean;
const host = USE_TLS ? "https://localhost:9091" : "http://localhost:9090";

function getBook() {

    ///
    // EXAMPLE OF UNARY REQUEST/RESPONSE
    ///
  const getBookRequest = new GetBookRequest();
  getBookRequest.setIsbn(60929871);
  grpc.unary(BookService.GetBook, {
    request: getBookRequest,
    host: host,
    onEnd: res => {
      const { status, statusMessage, headers, message, trailers } = res;
      console.log("getBook.onEnd.status", status, statusMessage);
      console.log("getBook.onEnd.headers", headers);
      if (status === grpc.Code.OK && message) {
        console.log("getBook.onEnd.message", message.toObject());
      }
      console.log("getBook.onEnd.trailers", trailers);
      queryBooks();
    }
  });
}

getBook();

function queryBooks() {

    ///
    // EXAMPLE OF UNARY REQUEST W/ STREAM RESPONSE
    ///
  const queryBooksRequest = new QueryBooksRequest();
  queryBooksRequest.setAuthorPrefix("Geor");
  const client = grpc.client(BookService.QueryBooks, {
    host: host,
  });
  client.onHeaders((headers: grpc.Metadata) => {
    console.log("queryBooks.onHeaders", headers);
  });
  client.onMessage((message: Book) => {
    console.log("queryBooks.onMessage", message.toObject());
  });
  client.onEnd((code: grpc.Code, msg: string, trailers: grpc.Metadata) => {
    console.log("queryBooks.onEnd", code, msg, trailers);
  });
  client.start();
  client.send(queryBooksRequest);
}

*/